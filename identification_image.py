import face_recognition
from PIL import Image, ImageDraw, ImageFont

# Definisanje fonta
font_path = "./fonts/OpenSans-ExtraBold.ttf"
fontSize = 15
font = ImageFont.truetype(font_path, fontSize)

# Ucitavanje slika osoba koje 'znamo'
image1 = face_recognition.load_image_file('./img/known/k3.jpg')
face_encoding1 = face_recognition.face_encodings(image1)[0]

image2 = face_recognition.load_image_file('./img/known/jodie5.jpg')
face_encoding2 = face_recognition.face_encodings(image2)[0]

image3 = face_recognition.load_image_file('./img/known/ariana1.jpg')
face_encoding3 = face_recognition.face_encodings(image3)[0]

image4 = face_recognition.load_image_file('./img/known/leo1.jpg')
face_encoding4 = face_recognition.face_encodings(image4)[0]

image5 = face_recognition.load_image_file('./img/known/tom1.jpg')
face_encoding5 = face_recognition.face_encodings(image5)[0]

image6 = face_recognition.load_image_file('./img/known/donald1.jpg')
face_encoding6 = face_recognition.face_encodings(image6)[0]

# Napraviti niz imena ljudi koje 'znamo' i niz karakteristika lica (face_encoding predstavlja niz koordinata karakteristika lica)
known_face_encodings = [
    face_encoding1,
    face_encoding2,
    face_encoding3,
    face_encoding4,
    face_encoding5,
    face_encoding6
]

known_face_names = [
    "Kylie Jenner",
    "Jodie Foster",
    "Ariana Grande",
    "Leonardo \n Dicaprio",
    "Tom Cruise",
    "Donald Trump"
]

# ucitati random sliku i izvrsiti detekciju (ako je ona moguca)
test_image = face_recognition.load_image_file('./img/unknown/collage6.jpg')

# pronaci lica na slici
face_locations = face_recognition.face_locations(test_image, model='cnn')
face_encodings = face_recognition.face_encodings(test_image, face_locations)


# prebaciti na PIL format
pil_image = Image.fromarray(test_image)

# napraviti ImageDraw instancu kako bi crtali na slici
draw = ImageDraw.Draw(pil_image)

# loop kroz lica u test slici
for(top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
    matches = face_recognition.compare_faces(
        known_face_encodings, face_encoding, tolerance=0.55)
    name = "Unknown person"

    if True in matches:
        first_match_index = matches.index(True)
        name = known_face_names[first_match_index]

        # crtanje okvira oko lica
        draw.rectangle(((left, top), (right, bottom)),
                       outline=(0, 255, 0), width=5)

        # nacrtati labelu
        text_width, text_height = draw.textsize(name)
        draw.rectangle(((left, bottom-text_height-5), (right, bottom+fontSize)),
                       fill=(0, 255, 0), outline=(0, 255, 0))
        draw.text((left+5, bottom-text_height-6), name,
                  fill=(255, 255, 255, 255), font=font)
    else:
        # crtanje okvira oko neprepoznatog lica
        draw.rectangle(((left, top), (right, bottom)),
                       outline=(255, 0, 0), width=5)
        text_width, text_height = draw.textsize(name)
        draw.rectangle(((left, bottom-text_height-5), (right, bottom+fontSize)),
                       fill=(255, 0, 0), outline=(255, 0, 0))
        draw.text((left+5, bottom-text_height-6), name,
                  fill=(255, 255, 255, 255), font=font)

del draw

# prikazati sliku sa PIL
# pil_image.show()

# sacuvati
pil_image.save('slika8.jpg')
