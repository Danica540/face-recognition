import face_recognition
# Face encoding vraca niz, nama treba sammo prvi element
image = face_recognition.load_image_file('./img/known/arnold1.jpg')
face_encoding = face_recognition.face_encodings(image)[0]

unknown_image2 = face_recognition.load_image_file('./img/known/arnold2.png')
face_encoding_unknown2 = face_recognition.face_encodings(unknown_image2)[0]

unknown_image = face_recognition.load_image_file('./img/known/arnold4.jpg')
face_encoding_unknown = face_recognition.face_encodings(unknown_image)[0]

# Uporedjujemo slicnost lica ljudi
results = face_recognition.compare_faces(
    [face_encoding], face_encoding_unknown)

print(results)

if results[0]:
    print('Lica se poklapaju')
else:
    print('Lica se ne poklapaju')
