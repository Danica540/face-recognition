import face_recognition

image = face_recognition.load_image_file('./img/ljudi3.jpg')
face_locations = face_recognition.face_locations(image)


# Niz koordinata svakog detektovanog lica
# top, right, bottom, left - struktura koordinata
print(face_locations)

# Detektuje koliko ima broj ljudi na slici
print(f'Na slici je {len(face_locations)} ljudi')
