import face_recognition
import cv2

# Uzmi instancu web camere
webcam_instance = cv2.VideoCapture(0)

# Ucitavanje slika koje 'znamo' i njihovih koordinata (encodings)
image1 = face_recognition.load_image_file('./img/known/d4.jpg')
face_encoding1 = face_recognition.face_encodings(image1)[0]

image2 = face_recognition.load_image_file('./img/known/jodie3.jpg')
face_encoding2 = face_recognition.face_encodings(image2)[0]

# Pravimo niz imena i koordinata lica
known_face_encodings = [
    face_encoding1,
    face_encoding2
]

known_face_names = [
    "Danica Djordjevic",
    "Jodie Foster"
]

# Inicijalizujemo promenljive
face_locations = []
face_encodings = []
face_names = []
process_this_frame = True

while True:
    # Uzimamo jedan frejm videa
    # ret se ne koristi, webcam_instance vraca 2 vrednost, nama treba samo frame
    ret, frame = webcam_instance.read()

    # Smanjujemo velicinu frejma (uzimamo jednu petinu) da bi procesiranje bilo brze
    resized_frame = cv2.resize(frame, (0, 0), fx=0.20, fy=0.20)

    # Konvertujemo iz bgr formata (koji OpenCV koristi) u rgb format (koji face_recognition koristi)
    rgb_resized_frame = resized_frame[:, :, ::-1]

    # Procesira se svaki drugi frejm, da bi se sacuvalo na vremenu
    if process_this_frame:
        # Nalazimo sva lica i koordinate lica na frejmu
        face_locations = face_recognition.face_locations(
            rgb_resized_frame, model='hog')
        face_encodings = face_recognition.face_encodings(
            rgb_resized_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # SUporenjujemo nadjena lica sa licima koje vec 'znamo'
            matches = face_recognition.compare_faces(
                known_face_encodings, face_encoding)
            name = "Unknown person"

            # # If a match was found in known_face_encodings, just use the first one.
            if True in matches:
                first_match_index = matches.index(True)
                name = known_face_names[first_match_index]

            face_names.append(name)

    process_this_frame = not process_this_frame

    # Crtanje na frejmu i prikazivanje
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Posto smo smanjili frejm, moramo da ga resize-ujemo
        top *= 4
        left *= 4
        right *= 6
        bottom *= 6

        if name != "Unknown person":
             # Cra kocku oko lica
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 0), 2)

            # Crta labelu sa imenom osobe
            cv2.rectangle(frame, (left, bottom - 35),
                          (right, bottom), (0, 255, 0), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6),
                        font, 1.0, (255, 255, 255), 1)
        else:
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            cv2.rectangle(frame, (left, bottom - 35),
                          (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6),
                        font, 1.0, (255, 255, 255), 1)

    # Prikazivanje izmenjenog frejma
    cv2.imshow('Video', frame)

    # Kad se pritisne q, aplikacija se ugasi
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Prekini da kontrolises kameru
webcam_instance.release()
cv2.destroyAllWindows()
